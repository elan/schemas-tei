const handler = {
    // Génerer les cartes de chaque projet
    generateCardProjet: function (projet) {
        // fonction pour la classe css
        function getClassStateData() {
            switch (projet.dataState) {
                case 'tiede':
                    return 'card-tiede';
                case 'froid':
                    return 'card-froid';
                case 'chaud':
                    return 'card-chaud';
                default:
                    return ''; // si le dataState n'est pas definit le resultat est vide
            }
        }

        function generateVitrineLink(id) {
            if (projet.id) {
                return `<a class="link-secondary" href="https://www.elan-numerique.fr/#${projet.id}" target="_blank">
                          <i class="fa-solid fa-circle-info fa-xs"></i>
                        </a>`;
            }
            return '';
        }

        // html de la carte
        const cardHTML = `
            <div class="col-md-4 mb-4">
                <div class="card bg-light ">
                    <div class="card-body">
                        <a class="link-secondary" href="${projet.linkUrl}" target="_blank">
                            <h5 class="card-title">
                                <img src="assets/icons/thermometer-half.svg" alt="thermometer" width="20" height="20" class="${getClassStateData()}">
                                ${projet.nom} 
                                ${generateVitrineLink(projet.id)}
                            </h5> 
                        </a>
                        <p class="card-text">${projet.description}</p>
                        <div class="btn-group btn-group-sm" role="group" aria-label="Documentation">
                            ${projet.linkODD ? ` <button type="button" class="btn btn-light"><a class="" href="https://gricad-gitlab.univ-grenoble-alpes.fr/elan/schemas-tei/-/raw/main/${projet.linkODD}">ODD</a></button>` : ''}
                            ${projet.linkManuel ? ` <button type="button" class="btn btn-light"><a class="" target="_blank" href="${projet.linkManuel}">Manuel</a></button>` : ''}
                            ${projet.linkTableau ? ` <button type="button" class="btn btn-light"><a class="" target="_blank" href="${projet.linkTableau}">Tableau</a></button>` : ''}
                            ${projet.linkDocTei ? ` <button type="button" class="btn btn-light"><a class="" target="_blank" href="${projet.linkDocTei}">Documentation</a></button>` : ''}
                        </div>
                    </div>
                </div>
            </div>
        `;

        return cardHTML;
    },

    // Génère la date de dernière modification
    setLastModifiedDate: function () {
        const lastModified = new Date(document.lastModified);
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        document.getElementById('modification-date').textContent = lastModified.toLocaleDateString('fr-FR', options);
    },

    // initialisation de la page
    init: function () {
        fetch('./assets/data/projets.json')
            .then((response) => response.json())
            .then((projets) => {
                projets.sort((a, b) => a.nom.localeCompare(b.nom));
                const projetsContainer = document.getElementById("projets");
                projets.forEach(projet => {
                    const cardHTML = handler.generateCardProjet(projet);
                    projetsContainer.innerHTML += cardHTML;
                });
                handler.setLastModifiedDate();
            }
        );
    }
}

window.addEventListener("load", (event) => {
    handler.init()
});
