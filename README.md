Ce dépôt contient les schémas TEI (sous la forme d'ODD et d'RNG) pour les différents projets accompagnés par ELAN (UMR Litt&Arts). Le vue d'ensemble de nos projets, ODD et manuel est visible graĉe à gitlab page ici : **https://elan.gricad-pages.univ-grenoble-alpes.fr/resources-tei/schemas-tei**

De plus, **une documentation ODD** est disponible sur le wiki : **https://gricad-gitlab.univ-grenoble-alpes.fr/elan/odd/-/wikis/home**

Le dépôt est organisé ainsi :
- **un dossier par projet**, nommé selon l'acronyme ou le nom court du projet, par exemple `tacitus/`, et constitué ainsi :
```css
tacitus/
├── manuel /* dossier optionnel */
│   └── tacitus_manuel.html /* un manuel par odd */
├── model /* dossier optionnel */
│   └── /* un ou plusieurs fichiers .xml exemple */
├── out
│   └── tacitus_odd.rng /* un rng par odd */
└── tacitus_odd.odd /* plusieurs odd peuvennt être présents */
```
Si un projet est constitué de plusieurs sous projet, on priviligéra une forme composée projet-sousprojet, par exemple `translatoscope_taf`et `translatoscope_renard`. Le choix est donc fait de ne pas imbriquer de dossier de projet les uns dans les autres.
- **un dossier `common/`** pour des ODD communs à différents projets (par exemple pour l'encodage de page éditoriales)
- **un dossier `depot_odd/`** qui contient le code permettant d'afficher dans gitlab pages une vue de l'ensemble de nos ODD
