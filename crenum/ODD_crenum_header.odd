<?xml version="1.0" encoding="UTF-8"?>

<TEI xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Manuel d'encodage pour les différents headers du projet CreNum - Édition numérique de
          la <title>Chronique française</title> de Guillaume Cretin</title>
        <respStmt>
          <resp>création de l'odd</resp>
          <name>Fanny Corsi</name>
        </respStmt>
        <respStmt>
          <resp>inclusion des éléments à partir de l'ODD_crenum_commun</resp>
          <name>Anne Garcia Fernandez</name>
        </respStmt>
        <respStmt>
          <resp>structuration du manuel et explications</resp>
          <name>Ellen Delvallée</name>
        </respStmt>
      </titleStmt>
      <publicationStmt>
        <p>Version de décembre 2023</p>
      </publicationStmt>
      <sourceDesc>
        <p>Généré à partir de l'ODD_crenum_commun avec l'aide d'Anne Garcia Fernandez. Modifié,
          complété et documenté par Fanny Corsi avec l'aide d'Anne Garcia Fernandez et de Théo
          Roulet.</p>
      </sourceDesc>
    </fileDesc>
    <revisionDesc>
      <change when="20230822">inclusion dans le schéma des déclarations des modules TEI à partir de
        l'ODD_crenum_commun</change>
      <change when="20230824">inclusion dans le schéma des déclarations des classes d'attribut
        modifiées ou supprimées à partir de l'ODD_crenum_commun</change>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <!--###########################################################################################
              ATTENTION : À partir d'ici, tu peux créer des descriptions et des explications sur la
              manière dont tes groupes d'éléments sont divisés. La structure est celle d'un fichier 
              texte XML avec des div, des paragraphes et des listes. Chaque fois que tu souhaites 
              référencer les différents groupes d'éléments, tu devras utiliser l'élément <specGrpRef> 
              complété par un attribut @target qui pointe vers l'identifiant de ton groupe spécifique.
          ###########################################################################################-->
      <div>
        <div>
          <!-- Descriptions communes aux trois fichiers (dont un qui reste caché car inusité) -->
          <head>Informations générales sur la création des headers</head>
          <p>Le projet CreNum comporte plusieurs fichiers headers distincts : <list>
              <item>Un fichier header_manuscrits</item>
              <item>Un fichier header_liste_personnes</item>
            </list>Les headers visent à établir la liste d'un certain nombre d'informations sur la
            transcription : en l'occurrence, les manuscrits collationnés et les noms de personnes
            mentionnés dans la <title>Chronique</title>. Les éléments utilisés pour ces fichiers
            ainsi que leurs attributs sont décrits dans ce manuel. L'encodage des livres fait
            l'objet d'une autre documentation.<lb/>Les fichiers headers possèdent un certain nombre
            d'éléments communs, décrits ci-dessous.<lb/>Il ne sera guère utile d'intervenir dans
            cette partie du header, sauf à préciser dans des balises <gi>respStmt</gi> la nature du
            travail effectué (balise <gi>resp</gi>) et le nom de son auteur (balise
            <gi>name</gi>).</p>
          <specGrpRef target="#global"/>
        </div>

        <div>
          <!-- Manuel Manuscrits -->
          <head>Fichier header décrivant les différents manuscrits</head>
          <p>Le fichier header_manuscrits contient une description des manuscrits utilisés dans le
            projet CreNum. Ce fichier permet de créer, sur le site Chroniques rimées, une page
            bibliographique dont l’objectif à terme est de décrire et de classer, de façon
            exhaustive, chaque manuscrit contenant une copie de la <title>Chronique
              française</title> de Guillaume Cretin.<lb/>L'encodage des informations sur les
            manuscrits obéit à des règles strictes : la hiérarchisation et l'ordre des informations
            sont contraints. Le manuel va en décrire les sections tour à tour.</p>
          <specGrpRef target="#manuscrit"/><div>
            <head>Identification du manuscrit : localisation et cote</head>
            <specGrpRef target="#ms_identification"/>
          </div>
          <div>
            <head>Contenu du manuscrit : repérage des feuillets contenant le texte d'un ou plusieurs
              livres de la <title>Chronique</title></head>
            <specGrpRef target="#ms_content"/>
          </div>
          <div>
            <head>Description matérielle du manuscrit : support et mise en page, écriture,
              éventuelles décorations</head>
            <specGrpRef target="#ms_physicalDescr"/>
          </div>
          <div>
            <head>Histoire du manuscrit : date de réalisation et autres informations de provenance
              si elles sont connues</head>
            <specGrpRef target="#ms_history"/>
          </div>
          <div>
            <head>Informations complémentaires : éventuelles numérisation et références
              bibliographiques</head>
            <specGrpRef target="#ms_addInfo"/>
          </div>
          <div>
            <p>Remarque : il est possible, dans ces balises, d'ajouter des paragraphes de
              commentaires grâce à la balise <gi>p</gi>.</p>
          </div>
        </div>

        <div>
          <!-- Manuel Liste de personnes -->
          <head>Fichier header comprenant les listes de personnes et les listes de relations</head>
          <p>Le fichier header_liste_personnes identifie, répertorie, classe et met en relation les
            données recueillies dans les balises <gi>persName</gi> des livres de la <title>Chronique
              française</title>. Il devient ensuite possible de transformer ces listes en différents
            outils critiques facilitant la lecture de la <title>Chronique</title> : informations sur
            les personnes mentionnées par Cretin par l’affichage d’une fenêtre au survol du texte,
            création d’un index de personnes, création d’une généalogie des rois de France
            permettant de visualiser les successions complexes, création d’une frise chronologique
            des règnes permettant de situer les actions des différents rois dans le temps.<lb/>Le
            manuel décrit d'abord le fonctionnement du header et ses balises ; il propose ensuite
            des éléments méthodologiques pour un traitement semi-automatisé des informations sur les
            personnes nommées dans la <title>Chronique française</title></p>

          <div>
            <head>Éléments présents dans les listes de personnes</head>
            <p>C’est à partir de cette liste qu’est créé l’index des noms de la
                <title>Chronique</title>. Les informations minimales continues dans les listes de
              personnes servent à alimenter les fenêtres qui apparaissent au survol des noms de
              personnes dans la version semi-modernisée du texte.</p>
            <specGrpRef target="#liste_personnes"/>
          </div>
          
          <div>
            <head>Éléments présents dans la liste de relations</head>
            <p>Ces éléments permettent de créer des visualisations de la succession des rois de
              France (arbre généalogique, frise) telle qu’elle figure dans la
                <title>Chronique</title> de Cretin.</p>
            <specGrpRef target="#liste_relations"/>
            <figure>
              <figDesc>Exemple de visualisation issue de la liste des relations, sous forme d'arbre
                généalogique.</figDesc>
              <graphic url="img/genealogy.png"/>
            </figure>
          </div>
          
          <div>
            <head>Méthode de recueil et d'enrichissement semi-automatique des informations sur les
              personnes nommées dans la <title>Chronique française</title></head>
            <p>Le fichier header_liste_personnes contient trois listes de personnes ainsi qu'une
              liste de relations existantes entre les membres de la famille royale. La création des
              listes de personnes a demandé plusieurs étapes de travail que nous explicitons ci-après.<list>
                <item>Création d'un tableau par livre lors de l'encodage des <gi>persName</gi>. Ces
                  tableaux contiennent le nom dans le texte, le nom normalisé, l'identifiant, l'URL
                  VIAF, l'URL biblissima et l'URL Wikidata.</item>
                <item>Extraction des données de Wikidata par Théo Roulet. Enrichissement des
                    <gi>listPerson</gi> et <gi>listPlace</gi> avec Wikidata. <p>L'alignement manuel
                    des noms de personnes et des noms de lieux au moment de l'encodage permet de
                    réinterroger facilement wikidata pour en extraire des informations
                    supplémentaires.</p><p>Wikidata est une base de données collaborative créée par
                    Wikimedia Deutschland pour réunir des informations encyclopédiques sous la forme
                    de données structurées. Ces dernières ont vocation à enrichir et harmoniser
                    l'information de wikipedia (grâce à leur forme davantage structurée et
                    potentiellement davantage référencée), mais elles sont également librement mises
                    à disposition de tous.</p><p>Crenum puise dans ces données afin d'enrichir le
                    lexique des lieux de coordonnées géographiques (latitude et longitude) et le
                    lexique des personnes de quelques informations biographiques simples pour les
                    remettre en contexte.</p>
                  <list>
                    <item>Ces informations sont récupérées au format CSV (tableur) en interrogeant
                      le point d'accès SPARQL de wikidata : <ref target="https://query.wikidata.org"
                        >https://query.wikidata.org</ref>
                    </item>
                    <item>Les requêtes, en langage SPARQL, sont conservées sur : <ref
                        target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/home"
                        >https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/home</ref>. Ces requêtes
                      sont facilement adaptables d'un livre à l'autre. Elles se basent sur la liste
                      des URI wikidata réunis dans les tableaux : il suffit donc de copier coller
                      cette liste d'URI dans la section de la requête (signalée à l'aide de
                      commentaire).</item>
                    <item>Les personnes recensées sont séparées en trois groupes correspondant au
                      niveau de détail des informations recherchées à leur sujet dans wikidata
                      (indiqué par la colonne du tableau : autre, minimal, maximal). <list>
                        <item>Autre : nom et prénom, description en français ou en anglais si
                          manquante en français. Ces informations sont récupérées par la requête :
                            <ref
                            target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation#personnes-de-la-cat%C3%A9gorie-autre"
                            >Autre</ref>
                        </item>
                        <item>Minimal : nom et prénom, description en français ou en anglais si
                          manquante en français, genre, date de naissance et de mort (en extrayant
                          également les références). Ces informations sont récupérées par la requête
                          : <ref
                            target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation#personnes-de-la-cat%C3%A9gorie-minimale"
                            >Minimal</ref>
                        </item>
                        <item>Maximal : <list>
                            <item>les informations biographiques des personnes de cette liste sont
                              récupérées à l'aide de la requête précédente : <ref
                                target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation#1-informations-biographiques"
                                >Maximal_bio</ref>
                            </item>
                            <item>3 autres requêtes sont également utilisées à leur sujet : <list>
                                <item>pour récupérer l'ensemble des titres et/ou positions (règnes)
                                  ainsi que les dates et la liste des éventuels prédécesseurs et
                                  successeurs pour chacun : <ref
                                    target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation#2-titres-et-positions"
                                    >Maximal_titres</ref></item>
                                <item>pour récupérer la liste des conjoints / conjointes et les
                                  dates de mariage / concubinage : <ref
                                    target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation#3-liste-des-conjoints-conjointes"
                                    >Maximal_conjoints</ref></item>
                                <item>pour récupérer la liste des parents de chaque personne de la
                                  liste (nom, prénom, genre et description du parent) : <ref
                                    target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation#4-informations-sur-les-parents"
                                    >Maximal_parents</ref></item>
                              </list>
                            </item>
                          </list>
                        </item>
                      </list>
                    </item>
                    <item>Les résultats relatifs aux personnes nécessitent une étape supplémentaire
                      de sélection / nettoyage avant d'être retransformés en TEI. Wikidata est
                      susceptible de conserver plusieurs informations contradictoires, notamment à
                      propos des dates de vie et d'activités des personnes, en fonction des sources.
                      Ce sont les responsables scientifiques du projet qui tranchent alors
                      lesquelles leur semblent les plus fiables (il s'agit souvent de différences de
                      précision ou de calendrier pris en compte). </item>
                    <item>La structure des fichiers CSV (tableurs) de résultat est expliquée sur
                        <ref
                        target="https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation"
                        >https://gitlab.com/litt-arts-num/crenum-v2/-/wikis/crenum-sparql-documentation</ref>
                    </item>
                  </list>
                </item>
                <item>Nettoyage des tableaux par Ellen Delvallée et Antoine Brix. Regroupement des
                  données extraites de Wikidata par Théo Roulet avec les données déjà présentes dans
                  les tableaux des livres I et II et nettoyage des données.</item>
                <item>Création semi-automatique des trois listes de personnes en fonction du
                    <att>type</att> à l'aide d'une formule de concaténation dans un tableau
                  excel.</item>
                <item>Création manuelle de la liste de relations.</item>
              </list> Ce fichier permet de créer, sur le site <title>Chroniques rimées</title>, une
              page avec un index des personnages ainsi qu'une page avec différentes visualisations
              des généalogies des rois des Francs et du partage du royaume.</p>
          </div>
          
        </div>

        <div type="irrelevent">
          <!-- Manuel facsimiles (à dissimuler car inusité) -->
          <head>Fichier header avec des informations sur les enluminures</head>
          <p>Le fichier header_facsimiles est généré à partir du manisfest disponible sur mandragore
            grâce au script jsonAnnotation2teiFacsimile.bash.</p>
          <specGrpRef target="#header_facsimile"/>
        </div>
      </div>
      <anchor xml:id="top"/>

      <!-- MODULES UTILISES -->
      <div>
        <schemaSpec ident="crenum" start="TEI" prefix="tei_" targetLang="fr" docLang="fr">
          <!--###########################################################################################
              ATTENTION : À partir d'ici, tu peux créer des groupes d'éléments inclus à l'intérieur de 
              la balise <specGrp>. Pour chaque <specGrp>, tu devras spécifier l'élément correct à l'aide :
              de xi:include et définir l'élément à appeler grâce à l'attribut @xpointer pointant vers la 
              valeur de l'xml:id de l'élément présent dans le fichier ODD_crenum_commun.
              
              Par exemple, voici l'élément présent dans ODD_commun :
                <elementSpec ident="text" mode="change" xml:id="text">
                  <desc>L'élément <gi>text</gi> est obligatoire, il contient l'élément <gi>body</gi>.</desc>
                </elementSpec>
             et comment le référencer :
             <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="text"/>). 
             
             Chaque groupe doit être complété par un attribut xml:id qui permettra de le référencer 
             lors de la structuration du manuel.
          ###########################################################################################-->

          <!-- Inclusion des <moduleRef> -->
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_tei"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_header"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_core"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_textstructure"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_transcr"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_textcrit"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_linking"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_namesdates"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_analysis"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_msdescription"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="mod_spoken"/>

          <!-- Déclaration des groupes et inclusion des déclarations d'éléments <elementSpec> -->
          <specGrp xml:id="global">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="TEI"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="teiHeader"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="fileDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="titleStmt"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="title_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="author_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="name"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="respStmt"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="resp"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="editionStmt"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="edition"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="publicationStmt"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="p_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="sourceDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="date_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="note_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="text"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="body_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="standOff"/>
          </specGrp>

          <specGrp xml:id="liste_personnes">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="listPerson"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="person"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="persName_header"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="idno_liste_personnes"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="birth"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="death"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="occupation"/>
          </specGrp>
          <specGrp xml:id="liste_relations">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="listRelation"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="relation"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="desc"/>
          </specGrp>

          <specGrp xml:id="manuscrit">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="msDesc"/>
          </specGrp>

          <specGrp xml:id="ms_identification">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="msIdentifier"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="settlement"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="repository"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="idno_manuscrits"/>
          </specGrp>

          <specGrp xml:id="ms_content">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="msContents"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="msItem"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="locus"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="author_manuscript"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="title_manuscript"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="rubric"/>
          </specGrp>

          <specGrp xml:id="ms_physicalDescr">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="physDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="objectDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="supportDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="support"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="extent"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="dimensions"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="height"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="width"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="collation"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="layoutDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="layout"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="handDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="decoDesc"/>
          </specGrp>

          <specGrp xml:id="ms_history">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="history"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="origDate"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="origin"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="provenance"/>
          </specGrp>

          <specGrp xml:id="ms_addInfo">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="additional"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="surrogates"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="ref"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="listBibl"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="bibl"/>
          </specGrp>


          <specGrp xml:id="fichiers_inclus">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="encodingDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="appInfo"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="application"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="label"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="profileDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="revisionDesc"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="listChange"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="change"/>
          </specGrp>

          <specGrp xml:id="header_facsimile">
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="facsimile"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="note_facsimile"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="surface"/>
            <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="zone"/>
          </specGrp>

          <!-- Inclusion des groupes dans le schéma -->
          <specGrpRef target="#global"/>
          <specGrpRef target="#liste_personnes"/>
          <specGrpRef target="#liste_relations"/>
          <specGrpRef target="#manuscrit"/>
          <specGrpRef target="#ms_identification"/>
          <specGrpRef target="#ms_addInfo"/>
          <specGrpRef target="#ms_history"/>
          <specGrpRef target="#ms_physicalDescr"/>
          <specGrpRef target="#ms_content"/>
          <specGrpRef target="#header_facsimile"/>
          <!-- Inclusion des classes d'attributs (supprimées ou modifiées) -->
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.anchoring"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.ascribed.directed"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.canonical"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.ranging"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.dimensions"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.written"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.damaged"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.cReferencing"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.datable"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.datcat"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.declaring"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.global.responsibility"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.editLike"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.global.rendition"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.global"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.handFeatures"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.internetMedia"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.media"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.resourced"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.interpLike"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.measurement"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.naming"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.notated"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.typed"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.pointing"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.pointing.group"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.scoping"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.segLike"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.sortable"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.edition"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.styleDef"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.timed"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.transcriptional"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.citing"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.formula"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.locatable"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.partials"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.duration.iso"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.citeStructurePart"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.patternReplacement"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.milestoneUnit"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml"
            xpointer="att.lexicographic.normalized"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.linguistic"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.global.analytic"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.global.linking"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.datable.custom"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.datable.iso"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.rdgPart"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.textCritical"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.msExcerpt"/>
          <xi:include href="ODD_crenum_commun.odd" parse="xml" xpointer="att.msClass"/>
        </schemaSpec>
      </div>
    </body>
  </text>
</TEI>
